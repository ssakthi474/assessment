<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('12345'); // Default user password

        $d = [

            ['name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => $password,
                'role' => config('constant.role.admin'),
                'status' => 1,
                'remember_token' => Str::random(10),
            ],
            ['name' => 'user',
            'email' => 'user@gmail.com',
            'password' => $password,
            'role' => config('constant.role.user'),
            'status' => 1,
            'remember_token' => Str::random(10),
            ],

        ];
        DB::table('users')->insert($d);
    }
}
