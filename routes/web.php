<?php

use App\Http\Controllers\OrganizationController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'OrganizationController', 'prefix' => 'oraganization'], function () {
        Route::get('index', [OrganizationController::class,'index'])->name('oraganization.index');
        Route::post('list', [OrganizationController::class,'list'])->name('oraganization.list');
        Route::post('store', [OrganizationController::class,'store'])->name('organization.store');
        Route::get('get', [OrganizationController::class,'getData'])->name('organization.get');
        Route::delete('delete', [OrganizationController::class,'destroy'])->name('organization.destroy');
        
    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
