<?php

use App\Http\Controllers\api\auth\RegisterController;
use App\Http\Controllers\api\OraganizationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




    Route::middleware('auth:api')->group(function () {
        Route::get('org/list', [OraganizationController::class,'list']);
        Route::post('org/store', [OraganizationController::class,'store']);
        Route::get('org/get/{oid}', [OraganizationController::class,'getData']);
        Route::delete('org/delete/{oid}', [OraganizationController::class,'destroy']);
    });
    
    
    Route::post('register', [RegisterController::class,'register'])->name('api.register');
    Route::post('login', [RegisterController::class,'login'])->name('api.login');
    Route::post('logout', [RegisterController::class,'logout']);
