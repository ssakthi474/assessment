@extends('layouts.app')

@section('content')

  
<div class="container col-md-12">
    <div class="d-flex">
        <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#storeOraganizationModal">
            Add Organization
          </button>
    </div>
    <div class="table-responsive">
        <table class="table datatable" id="organizationTable">
            <thead>
                <th>Organization Name</th>
                <th>Created By</th>
                <th>Status</th>
                <th>Action</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>


{{-- modal --}}
  <div class="modal fade" id="storeOraganizationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        
        <form id="storeOrganization">
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="hidden" class="form-control"  name ="id" id="id" >
                    <input type="text" class="form-control"  name ="name" id="name" placeholder="Organization name ">
                  </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger close-modal" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" >Submit</button>
                </div>
            </form>
      </div>
    </div>
  </div>
@endsection
@push('js')
<script>
    var table = $('#organizationTable').DataTable({
            'ordering': false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{route('oraganization.list')}}",
                "type": "POST",
                data: function (d) {
                  
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }

            },
            "columns": [{
                    "name": "name",
                    "data":"name"
                    },
                    {
                      "name":"created by" ,
                      "data":"user.name" 
                    }, {
                    "name": "status",
                    "render": function (data,type,row){
                        let html = "";
                            if(row.status == 1)
                            {
                           html = `<span class="badge badge-success">Active</span>`     
                            }else{
                                html = `<span class="badge badge-warning">Deactive</span>`     
                            }
                       
                        return html;
                    }
                },
                {
                    "mRender": function (data, type, row) {

                        var btn = '';
                        
                        btn = `<div class="list-icons">
                                    
                                        <div class="d-flex">

                                            <a href="javascript:void(0);" class="edit btn btn-primary mr-1" attr="${row.id}">Edit</a>
                                            <a href="javascript:void(0);" class="delete btn btn-danger " attr="${row.id}">Delete</a>
                                           
                                        </div>
                                 
                                </div> `;
                        
                        return btn;
                    }
                }
            ]

        });


        $("#storeOrganization").validate({
        rules: {
            name : {
                required : true,
            },
        },
        errorPlacement: function (error, element) {
            
            if (element.hasClass("select2-hidden-accessible")) {
                error.insertAfter(element.siblings('span.select2'));
            } else if (element.hasClass("floating-input")) {
                element.closest('.form-floating-label').addClass("error-cont").append(error);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {

            
                var data = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{route('organization.store')}}",
                    data: data,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (response) {
                        if(response.success == 1)
                        {
                            alert(response.message);
                            $('#storeOraganizationModal').modal('hide');
                            $('.close-modal').trigger('click');
                            table.draw();
                        }else{
                            alert(response.message);
                        }
                    }
                });
            
        }
    });


    $(document).on('click','.edit',function(){
        var id = $(this).attr('attr');
        $.ajax({
                    type: "get",
                    url: "{{route('organization.get')}}",
                    data: {
                        id:id
                    },
                    dataType: "json",
                    success: function (response) {
                        if(response.success == 1)
                        {
                            $('#storeOraganizationModal #storeOrganization #id').val(response.data.id)
                            $('#storeOraganizationModal #storeOrganization #name').val(response.data.name)
                            $('#storeOraganizationModal').modal('show');
                        }else{
                            alert(response.message);
                        }
                    }
                });
    });
    $(document).on('click','.delete',function(){
        var id = $(this).attr('attr');
        $.ajax({
                    type: "DELETE",
                    url: "{{route('organization.destroy')}}",
                    data: {
                        id:id
                    },
                    dataType: "json",
                    success: function (response) {
                        if(response.success == 1)
                        {
                            alert(response.message);
                            table.draw();
                        }else{
                            alert(response.message);
                        }
                    }
                });
    });
 
</script>
@endpush