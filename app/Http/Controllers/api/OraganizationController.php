<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OraganizationController extends Controller
{
    //

    public function list()
    {
        if(auth()->user()->role == config('constant.role.admin'))
        {
            $data =Organization::with('user')->where('status',1)->get();
            
        }else{
            $data = Organization::with('user')->where('status',1)->where('user_id',auth()->user()->id)->get();
        }
        $response_data = ["success" => 1 , "message" => "List Get Successfully" , "data" => $data ];
        
        return response()->json($response_data);
    }
    public function store(Request $req)
    {
        $validation = Validator::make($req->all(), [
            "name" => "required"
        ]);
        if (!$validation->fails()) {

                $org = Organization::where('id',$req->id)->first();
                if(!$org)
                $org = new Organization();
                $org->name = $req->name;
                $org->user_id = auth()->user()->id;
                $org->status = 1;
                if($org->save())
                {
                    $response_data = ["success" => 1, "message" => "Organization Added Successfully", "data" => $org, ];
                } else {
                    $response_data = ["success" => 0, "message" => "Site Server Error"];
                }
        }else {
            $response_data = ["success" => 0, "message" => "Validation Error"];
        }

        return response()->json($response_data);
    }

    public function getData($id)
    {
        if($id != "")
        {
            $org = Organization::where('id',$id)->first();
            if($org)
            {
                $response_data = ["success" => 1, "message" => "Organization get Successfully", "data" => $org, ];
            } else {
                $response_data = ["success" => 0, "message" => "Site Server Error"];
            }
        }else{
            $response_data = ["success" => 0, "message" => "Site Server Error"];
        }
        return response()->json($response_data);
    }


    public function destroy($id)
    {
        if($id != "")
        {
            $org = Organization::where('id',$id)->delete();
            if($org)
            {
                $response_data = ["success" => 1, "message" => "Organization Deleted Successfully", ];
            } else {
                $response_data = ["success" => 0, "message" => "Site Server Error"];
            }
        }else{
            $response_data = ["success" => 0, "message" => "Site Server Error"];
        }
        return response()->json($response_data);
    }
}
