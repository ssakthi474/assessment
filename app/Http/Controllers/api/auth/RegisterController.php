<?php

namespace App\Http\Controllers\api\auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    
    public function store(Request $req)
    {
        $validation = Validator::make($req->all(), [
            "name" => "required",
            "email" =>"required | email",
            "password" =>  "required",
            "c_password" => "required | same:password"
        ]);
        if (!$validation->fails()) {

            
                $user = new User();
                $user->name = $req->name;
                $user->email = $req->email;
                $user->role = $req->role;
                $user->password =Hash::make($req->password);;
                $user->status = 1;
                if($user->save())
                {   
                    $user = User::with('organizations')->where('id',$user->id)->first();
                    $token =  $user->createToken('test');
                    $response_data = ["success" => 1, "message" => "Registered Successfully", "data" => $user,"token"=>$token->accessToken ];
                } else {
                    $response_data = ["success" => 0, "message" => "Site Server Error"];
                }
        }else {
            $response_data = ["success" => 0, "message" => "Validation Error"];
        }

        return response()->json($response_data);
    }

    public function login(Request $req)
    {
        $validation = Validator::make($req->all(), [
            "email" =>"required ",
            "password" =>  "required",
        ]);
        if (!$validation->fails()) {

            if(Auth::attempt(['email' => $req->email , 'password' => $req->password ]))
            {
                $user = Auth::user();
                $token = $user->createToken('test');
                $user = User::with('organizations')->where('id',$user->id)->first();
                $response_data = ["success" => 1, "message" => "Login Successfully", "data" => $user,"token"=>$token->accessToken ];

            }else{
                $response_data = ["success" => 0, "message" => "Invalid Login", "data" => "" ];

            }
        }else{
            $response_data = ["success" => 0, "message" => "Validation Error"]; 
        }

        return response()->json($response_data);
    }

    public function logout(Request $req)
    {
        
        Auth::logout();
       $response_data = ["success" => 1, "message" => "Logout Successfully"]; 
       return response()->json($response_data);
    }
}
